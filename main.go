// Copyright (c) 2020 Ofte LLC
// This file is open source, so have at it. Note however that it is
// subject to the terms and conditions defined in the file LICENSE.
package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	googletoken "github.com/avivklas/GoogleIdTokenVerifier"
	"github.com/fraugster/cli"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/prologic/bitcask"
)

/*
	A simple, one-pager web backend service that:

	- returns inspiration quotes (read from a CSV)
	- provides for management of Acme's product items (managed in a simple KV store)

	Authentication is accomplished in the UI (via Google OAUTH). Authorization
	is enforced using middleware that valids Bearer tokens back to Google.
*/

type quote struct {
	Quote  string
	Person string
}

type product struct {
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Cost        float64 `json:"cost"`
	List        float64 `json:"list"`
	SKU         string  `json:"sku"`
}

var (
	quotes []quote
	db     *bitcask.Bitcask
	envMap = map[string]string{}
)

func main() {
	rand.Seed(time.Now().UnixNano())
	buildEnvMap()
	openQuoteFile()
	openDBFile()
	defer db.Close()
	router := setupWebServer()

	ctx := cli.Context()
	log.Println("HTTPS Listening on ", envMap["PORT"])
	go router.RunTLS(":"+envMap["PORT"], envMap["CERTIFICATE_FILE"], envMap["KEY_FILE"])
	<-ctx.Done()
}

func buildEnvMap() {
	var envKeys = []string{
		"PORT",
		"STATIC_FILES_LOCATION",
		"CERTIFICATE_FILE",
		"KEY_FILE",
		"QUOTES_FILE",
		"OAUTH_CLIENT_ID",
	}
	for _, key := range envKeys {
		val := os.Getenv(key)
		if val == "" {
			panic(errors.Errorf("Missing environment variable '%s'", key))
		}
		envMap[key] = val
		log.Println("Environment: ", key, ": ", val)
	}
}

func setupWebServer() *gin.Engine {
	router := gin.Default()

	router.Use(static.Serve("/", static.LocalFile(envMap["STATIC_FILES_LOCATION"], true)))
	router.GET("/env.js", handleJSEnv)

	group := router.Group("/api")
	{
		group.Use(googleAuthMiddleware())
		group.GET("/quote", getQuote)
		group.GET("/products", products)
		group.POST("/products", updateProducts)
	}

	return router
}

// getQuote returns a single, random quote. Authentication is
// protected by the google oauth middleware.
func getQuote(ctx *gin.Context) {
	index := rand.Int31n(int32(len(quotes)))
	ctx.AbortWithStatusJSON(http.StatusOK, quotes[index])
}

// products returns the (complete) product listing. Authentication is
// protected by the google oauth middleware.
func products(ctx *gin.Context) {
	b, err := db.Get([]byte("products"))
	if err != nil {
		_ = ctx.AbortWithError(500, err)
		return
	}
	var products []product
	err = json.Unmarshal(b, &products)
	if err != nil {
		_ = ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, products)
}

// updateProducts stores the passed (complete) product listing. Authentication is
// protected by the google oauth middleware.
func updateProducts(ctx *gin.Context) {
	var products []product
	err := json.NewDecoder(ctx.Request.Body).Decode(&products)
	if err != nil {
		_ = ctx.AbortWithError(400, err)
		return
	}
	b, err := json.Marshal(products)
	if err != nil {
		_ = ctx.AbortWithError(500, err)
		return
	}
	err = db.Put([]byte("products"), b)
	if err != nil {
		_ = ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, products)
}

// handleJSEnv is called by the web app to retrieve environment variables.
func handleJSEnv(ctx *gin.Context) {
	value := fmt.Sprintf(`
	function getEnvironmentVariable(envvar) {
		switch (envvar) {
		case 'OAUTH_CLIENT_ID':
			return '%s'
			break
		}
	}`,
		envMap["OAUTH_CLIENT_ID"])

	_, _ = ctx.Writer.WriteString(value)
	ctx.Header("Content-Type", "application/javascript")
	ctx.AbortWithStatus(200)
}

// openQuoteFile : static CSV of inspirational quotes.
func openQuoteFile() {
	csvFile, err := os.Open(envMap["QUOTES_FILE"])
	if err != nil {
		panic(errors.Wrapf(err, "opening quotes file %s", envMap["QUOTES_FILE"]))
	}
	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.Comma = ';'
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		quotes = append(quotes, quote{
			Quote:  line[0],
			Person: line[1],
		})
	}
}

// openDBFile : a simple KV store of Acme products.
func openDBFile() {
	var err error
	db, err = bitcask.Open("./products.db")
	if err != nil {
		panic(err)
	}
	_, err = db.Get([]byte("products"))

	// if no products, add some in
	if err == bitcask.ErrKeyNotFound {
		products := []product{
			{
				Name:        "Anvil",
				Description: "Solid iron, great for dropping from dizzying heights",
				Cost:        30.50,
				List:        99.99,
				SKU:         "acme/anvil",
			},
			{
				Name:        "Rocket-Powered Roller Skates",
				Description: "Let you skate at unlimited speed",
				Cost:        40.25,
				List:        109.99,
				SKU:         "acme/rocket-powered-roller-skates",
			},
			{
				Name:        "Triple-Strength Fortified Leg Muscle Vitamins",
				Description: "Gives your legs the vitamins it needs to run faster than ever before",
				Cost:        3.25,
				List:        6.99,
				SKU:         "acme/leg-muscle-vitamins",
			},
			{
				Name:        "Jet Bike (Kit)",
				Description: "Like a motorcycle, but without wheels",
				Cost:        543.22,
				List:        1199.99,
				SKU:         "acme/jet-bike-kit",
			},
			{
				Name:        "Dehydrated Boulders",
				Description: "Makes instant boulders with just a drop of water",
				Cost:        31.25,
				List:        79.99,
				SKU:         "acme/dehydrated-boulders",
			},
			{
				Name:        "Giant Rubber Band V2",
				Description: "Ideal for tripping road runners",
				Cost:        4.25,
				List:        9.99,
				SKU:         "acme/giant-rubber-band-v2",
			},
			{
				Name:        "Tornado Seeds",
				Description: "Guaranteed EF3 twister or your money back, just add water",
				Cost:        0.76,
				List:        2.99,
				SKU:         "acme/tornado-seeds",
			},
		}

		b, err := json.Marshal(products)
		if err != nil {
			panic(err)
		}
		err = db.Put([]byte("products"), b)
		if err != nil {
			panic(err)
		}
	}
}

// googleAuthMiddleware checks for the existence of a Google OAUTH Bearer
// token and validates that token (phones home to Google's OAUTH servers).
func googleAuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var (
			err   error
			code  int
			split []string
		)
		token := ctx.Request.Header.Get("Authorization")
		if token == "" {
			code = 400
			err = errors.New("missing authorization header")
			goto abort
		}
		split = strings.Split(token, " ")
		if len(split) != 2 {
			code = 400
			err = errors.New("authorization header incorrectly formatted")
			goto abort
		}
		_, err = googletoken.Verify(split[1], envMap["OAUTH_CLIENT_ID"])
		if err != nil {
			code = 401
			goto abort
		}

		ctx.Next()
		return

	abort:
		_ = ctx.AbortWithError(code, err)
	}
}
