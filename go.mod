module gitlab.com/ofte/example-corp-portal-vuejs

go 1.13

require (
	github.com/avivklas/GoogleIdTokenVerifier v0.0.0-20170621084245-0ac14b60f9df
	github.com/fraugster/cli v1.1.0
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-gonic/gin v1.5.0
	github.com/pkg/errors v0.9.1
	github.com/prologic/bitcask v0.3.5
)
