# example-portal-vuejs-golang
This is an example Vue.js+Golang app, used to demonstrate steps to integrate [Ofte](https://ofte.io) DogPark and Ofte Continuous Authentication.

## The Base Application
The `master` branch contains a small, sample application which is a basic Vuejs single page app. It implements ["Acme Corporation's"](https://en.wikipedia.org/wiki/Acme_Corporation) portal, through which employees can get inspirational quotes and manage Acme's product offerings.

The app uses Google OAUTH2 for <i>first factor authentication</i>. Any valid Google account will
let you into the app (and consider you an Acme employee, welcome aboard!). All server-side code is in the single Golang module: `main.go`. Authorization to the various resources of the app is protected by middleware in main.go that verifies the Bearer token provided by Gooogle (it phones home to their authentication services).


Demo: [https://portal.ofte.io](https://portal.ofte.io)


Running:

```
export PORT=8888
export	STATIC_FILES_LOCATION=web-app
export CERTIFICATE_FILE=certs/cert.pem
export KEY_FILE=certs/key.pem
export QUOTES_FILE=quotes.csv
export OAUTH_CLIENT_ID=<your google OAUTH client id>

git clone https://gitlab.com/ofte/example-portal-vuejs-golang
cd example-portal-vuejs-golang
go run main.go
```

Note that in order to run this yourself, you'll need to [set up](https://developers.google.com/identity/sign-in/web/sign-in) a Google project in order to get an OAUTH `client id` that you'll assign to the `OAUTH_CLIENT_ID` environment variable.

## A FIDO-protected Application
The `ofte-dogpark` branch contains the changes needed to the master branch to integrate [Ofte Dog Park](https://github.com/oftecorp/dogpark), our open source platform for:

- easily integrating webauthn/FIDO2 capabilities into a webapp
- managing the FIDO keys in use in your application

You can compare the changes between the branches [here(TODO)]().

Demo: [https://portal-dogpark.ofte.io-TODO]()<br>
Note: You'll need a FIDO/U2F-compliant key for this demo.

## A Continuous Authentication-protected Application
The `ofte-ca` branch contains the changes needed to the master branch to integrate Ofte CA, our platform that <strong>extends FIDO</strong> with a continuous authentication protocol that protects your data with a higher level of security than straight-up FIDO.

You can compare the changes between the branches [here(TODO)]().

Demo: [https://portal-ca.ofte.io-TODO]()<br>
Note: You'll need an Ofte FIDO key for this demo.

---
Copyright (c) 2020 Ofte LLC