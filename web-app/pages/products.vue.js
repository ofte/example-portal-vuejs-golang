var portalProducts = Vue.component("Products", {
  template: `  <div>
  <v-toolbar flat color="white">
    <v-toolbar-title>Acme Product Item Management</v-toolbar-title>
    <v-divider
      class="mx-2"
      inset
      vertical
    ></v-divider>
    <v-spacer></v-spacer>
    <v-dialog v-model="dialog" max-width="500px">
      <v-card>
        <v-card-title>
          <span class="headline">Edit Item</span>
        </v-card-title>

        <v-card-text>
          <v-container grid-list-md>
            <v-layout wrap>
              <v-flex xs12 sm6 md4>
                <v-text-field v-model="editedItem.name" label="Product Name"></v-text-field>
              </v-flex>
              <v-flex xs12 sm6 md4>
                <v-text-field v-model="editedItem.description" label="Description"></v-text-field>
              </v-flex>
              <v-flex xs12 sm6 md4>
                <v-text-field v-model="editedItem.cost" label="Cost (USD)"></v-text-field>
              </v-flex>
              <v-flex xs12 sm6 md4>
                <v-text-field v-model="editedItem.list" label="List (USD)"></v-text-field>
              </v-flex>
              <v-flex xs12 sm6 md4>
                <v-text-field v-model="editedItem.sku" label="SKU"></v-text-field>
              </v-flex>
            </v-layout>
          </v-container>
        </v-card-text>

        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="blue darken-1" flat @click="close">Cancel</v-btn>
          <v-btn color="blue darken-1" flat @click="save">Save</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
  </v-toolbar>
  <v-data-table
    :headers="headers"
    :items="products"
    :pagination.sync="pagination"
    class="elevation-1"
  >
    <template v-slot:items="props">
      <td>{{ props.item.name }}</td>
      <td class="text-xs-left">{{ props.item.description }}</td>
      <td class="text-xs-left">{{ props.item.cost }}</td>
      <td class="text-xs-left">{{ props.item.list }}</td>
      <td class="text-xs-left">{{ props.item.sku }}</td>
      <td class="justify-center layout px-0">
        <v-icon
          small
          class="mr-2"
          @click="editItem(props.item)"
        >
          edit
        </v-icon>
      </td>
    </template>
  </v-data-table>
</div>`,
  data: () => ({
    pagination: {
      rowsPerPage: 10
    },
    dialog: false,
    headers: [
      { text: 'Name', value: 'name' },
      { text: 'Description', value: 'description' },
      { text: 'Cost', value: 'cost' },
      { text: 'List', value: 'list' },
      { text: 'SKU', value: 'sku' },
      { text: 'Actions', value: 'name', sortable: false }
    ],
    products: [],
    editedIndex: -1,
    editedItem: {
      name: '',
      description: '',
      cost: 0,
      list: 0,
      sku: ''
    },
    defaultItem: {
      name: '',
      description: '',
      cost: 0,
      list: 0,
      sku: ''
    }
  }),

  watch: {
    dialog(val) {
      val || this.close()
    }
  },

  created() {
    this.initialize()
  },

  methods: {
    initialize() {
      this.getDataFromApi()
        .then(resp => resp.json())
        .then(json => { this.products = json })
    },

    getDataFromApi() {
      let url = "/api/products"
      return fetch(url, {
        headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('id_token') },
      })
        .catch(err => {
          sessionStorage.removeItem('id_token')
          router.push({ name: 'error', params: { err: err } })
        })
    },

    editItem(item) {
      this.editedIndex = this.products.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialog = true
    },

    close() {
      this.dialog = false
      setTimeout(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      }, 300)
    },

    save() {
      if (typeof this.editedItem.list === 'string') {
        this.editedItem.list = Number(this.editedItem.list)
      }
      if (typeof this.editedItem.cost === 'string') {
        this.editedItem.cost = Number(this.editedItem.cost)
      }
      Object.assign(this.products[this.editedIndex], this.editedItem)

      let url = "/api/products"
      fetch(url, {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + sessionStorage.getItem('id_token'),
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.products)
      })
        .catch(err => {
          sessionStorage.removeItem('id_token')
          router.push({ name: 'error', params: { err: err } })
        })

      this.close()
    }
  }
});