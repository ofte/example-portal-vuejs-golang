var portalLogin = Vue.component("Login", {
    template: `<v-dialog v-model="dialog" persistent max-width="500px">
    <v-card class="elevation-12">
        <v-toolbar dark color="primary">
            <v-toolbar-title>Acme Portal Login</v-toolbar-title>
            <v-spacer></v-spacer>
        </v-toolbar>
        <v-card-text>
            <v-stepper v-model="step" vertical>

                <v-stepper-step :complete="step > 1" step="1">
                    Sign in using Google OAUTH
                    <small>We'll let Google manage our authentication.</small>
                </v-stepper-step>
                <v-stepper-content step="1">
                    <g-signin-button :params="googleSignInParams" 
                        @success="onSignInSuccess" 
                        @error="onSignInError">
                    </g-signin-button>
                </v-stepper-content>

                <v-stepper-step :complete="step > 2" step="2">
                    Access Portal
                </v-stepper-step>
                <v-stepper-content step="2">
                    <v-progress-linear :indeterminate="true"></v-progress-linear>
                    Authenticated, accessing Portal now
                </v-stepper-content>

            </v-stepper>
        </v-card-text>
    </v-card>
</v-dialog>`,
    data() {
        return {
            dialog: true,
            step: 1,
            googleSignInParams: {
                client_id: getEnvironmentVariable('OAUTH_CLIENT_ID')
            },
            principal: {
                username: '',
                displayName: '',
                icon: ''
            }
        }
    },
    watch: {
        step: function () {
            switch (this.step) {
                case 1:
                    break
                case 2:
                    this.navigateToPortal()
                    break
            }
        }
    },
    mounted() {
    },
    methods: {
        onSignInSuccess(googleUser) {
            sessionStorage.setItem('id_token', googleUser.getAuthResponse().id_token)
            let profile = googleUser.getBasicProfile()
            this.principal = {
                username: profile.getEmail(),
                displayName: profile.getName(),
                icon: profile.getImageUrl()
            }
            document.dispatchEvent(new CustomEvent('login', { detail: this.principal }))
            this.step = 2
        },
        onSignInError(error) {
            console.log('Error with Google Authentication:', error)
            sessionStorage.removeItem('id_token')
            router.push({ name: 'error', params: { err: error } })
        },
        navigateToPortal() {
            // for animation effect
            setTimeout(function () {
                this.dialog = false
                router.push("/")
            }, 1000)
        },
    }
});
