var portalHome = Vue.component("Home", {
  template: `<div>
    <v-layout>
    <v-flex xs12 sm6 offset-sm3>
    <h2>Quotes of the Day</h2>
    <span>Get some inspiration to get some work done!<br><br></span>
      <v-card>
        <v-img
          src="/img/desert.jpg"
          aspect-ratio="2.75">
        </v-img>

        <v-card-title primary-title>
          <div>
            <h3 class="headline mb-0">{{quote}}</h3>
            <br/>
            <h4>-{{author}}</h4>
          </div>
        </v-card-title>

        <v-card-actions>
          <v-btn flat color="orange" v-on:click="loadQuote">New Quote</v-btn>
        </v-card-actions>
      </v-card>
    </v-flex>
  </v-layout>
  <v-snackbar v-model="snackbar" color="cyan darken-2" multi-line :timeout="0">
  This is the base example application that has no second factor authentication set. It's used as a basis to show how easy it is to integrate the Ofte authentication platforms.
  <v-btn dark flat @click="closeSnackbar()">
      Close
  </v-btn>
</v-snackbar>
</div>`,
  data() {
    return {
      quote: '',
      author: '',
      snackbar: sessionStorage.getItem('notice-closed') === null,
    };
  },
  mounted() {
    this.loadQuote()
  },
  methods: {
    
    loadQuote() {
      _this = this

      fetch('/api/quote', {
        headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('id_token') },
      })
        .then(resp => resp.json())
        .then(json => {
          this.quote = json.Quote
          this.author = json.Person
        })
        .catch((err) => {
          sessionStorage.removeItem('id_token')
          router.push({ name: 'error', params: { err: err } })
        })
    },

    closeSnackbar() {
      this.snackbar = false
      sessionStorage.setItem('notice-closed', 'true')
    }
  }
});